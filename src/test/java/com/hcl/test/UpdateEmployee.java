package com.hcl.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;

import com.hcl.dao.EmployeeDao;
import com.hcl.main.Main;
import com.hcl.model.Employee;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
public class UpdateEmployee {
    Main main;
    private int id;
    private String name;
    private int age;
    List<String> empList;
    @Given("^User wants to update an employee details$")
    public void user_wants_to_update_an_employee_details()throws Throwable {
           main = new Main();
           empList=new ArrayList<String>();
    }

    @When("^User enters the name as \"([^\"]*)\" and age as (\\d+) where id as (\\d+)$")
    public void user_enters_the_name_as_and_age_as_where_id_as(String name,Integer age,Integer id) throws Throwable {
        this.id = id;
        this.name = name;
        this.age = age;
        
    }
   /* @And("^Fetch data from database for updation$")
    public void fetch_data_from_database_for_updation(DataTable dt)
        {
        List<String> list = dt.asList(String.class);
        empList.add(list.get(0));
        empList.add(list.get(1));
        empList.add(list.get(2));
        }*/
    @Then("^Data successfully updated and it returns as (\\d+)$")
    public void data_successfully_updated_return_result_as(int result) throws Throwable {
       Assert.assertEquals(result,main.update(name, age, id));
       // Assert.assertEquals(3,empList.size());
        //Assert.assertEquals(empList.get(0),name);
        //Assert.assertEquals(Integer.parseInt(empList.get(1)),age);
        
    }
}
